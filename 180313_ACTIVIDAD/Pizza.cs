namespace _180313_ACTIVIDAD
{
    public abstract class Pizza
    {
        internal string name;
        public string Name {
            get { return name; }
        }

        public abstract void Prepare();
        public abstract void Bake();
        public abstract void Box();
    }
}
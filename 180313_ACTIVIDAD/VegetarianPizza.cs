using System;
namespace _180313_ACTIVIDAD
{
    public class VegetarianPizza : Pizza
    {
        public VegetarianPizza()
        {
            name = "Vegetarian Pizza";
        }

        public override void Prepare()
        {
            Console.WriteLine("Preparing Vegetarian Pizza!");
        }

        public override void Bake()
        {
            Console.WriteLine("Baking Vegetarian Pizza!");
        }

        public override void Box()
        {
            Console.WriteLine("Putting Vegetarian Pizza in a box!");
        }
    }
}
using System;

namespace _180313_ACTIVIDAD
{
    public class HipsterPizzaStore : PizzaStore
    {
        public override Pizza CreatePizza(string kind)
        {
            switch(kind)
            {
                case "Vegan":
                    return new VeganPizza();
                case "Vegetarian":
                    return new VegetarianPizza();
                default:
                    throw new ArgumentException();
            }
        }
    }
}
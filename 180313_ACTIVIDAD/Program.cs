﻿using System;

namespace _180313_ACTIVIDAD
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("I am a hipster pizza store");
            PizzaStore store = new HipsterPizzaStore();
            Pizza p;
            try {
                Console.WriteLine("Ordering vegetarian pizza");
                p = store.Order("Vegetarian");
                Console.WriteLine("{0} ordered!", p.Name);
                Console.WriteLine("Ordering vegan pizza");
                p = store.Order("Vegan");
                Console.WriteLine("{0} ordered!", p.Name);
                Console.WriteLine("Ordering pepperoni pizza");
                p = store.Order("Pepperoni");
                Console.WriteLine("{0} ordered!", p.Name);
            }
            catch (ArgumentException e) {
                Console.WriteLine("The requested item is not on the menu");
                System.Diagnostics.Debug.Print(e.StackTrace);
            }
            Console.WriteLine("Now I am a traditional pizza store");
            store = new TraditionalPizzaStore();
            try {
                Console.WriteLine("Ordering pepperoni pizza");
                p = store.Order("Pepperoni");
                Console.WriteLine("{0} ordered!", p.Name);
                Console.WriteLine("Ordering meat pizza");
                p = store.Order("Meat");
                Console.WriteLine("{0} ordered!", p.Name);
                Console.WriteLine("Ordering vegan pizza");
                p = store.Order("Vegan");
                Console.WriteLine("{0} ordered!", p.Name);
            }
            catch (ArgumentException e) {
                Console.WriteLine("The requested item is not on the menu");
                System.Diagnostics.Debug.Print(e.StackTrace);
            }
        }
    }
}

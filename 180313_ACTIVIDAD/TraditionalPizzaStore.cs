using System;

namespace _180313_ACTIVIDAD
{
    public class TraditionalPizzaStore : PizzaStore
    {
        public override Pizza CreatePizza(string kind)
        {
            switch(kind)
            {
                case "Pepperoni":
                    return new PepperoniPizza();
                case "Meat":
                    return new MeatPizza();
                default:
                    throw new ArgumentException();
            }
        }
    }
}
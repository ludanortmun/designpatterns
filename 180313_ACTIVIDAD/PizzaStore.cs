namespace _180313_ACTIVIDAD
{
    public abstract class PizzaStore
    {
        public Pizza Order(string kind)
        {
            Pizza p = CreatePizza(kind);
            p.Prepare();
            p.Bake();
            p.Box();
            return p;
        }

        public abstract Pizza CreatePizza(string kind);
    }
}
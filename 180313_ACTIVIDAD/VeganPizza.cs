using System;
namespace _180313_ACTIVIDAD
{
    public class VeganPizza : Pizza
    {
        public VeganPizza()
        {
            name = "Vegan Pizza";
        }

        public override void Prepare()
        {
            Console.WriteLine("Preparing Vegan Pizza!");
        }

        public override void Bake()
        {
            Console.WriteLine("Baking Vegan Pizza!");
        }

        public override void Box()
        {
            Console.WriteLine("Putting Vegan Pizza in a box!");
        }
    }
}
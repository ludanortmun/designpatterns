using System;
namespace _180313_ACTIVIDAD
{
    public class MeatPizza : Pizza
    {
        public MeatPizza()
        {
            name = "Meat Pizza";
        }

        public override void Prepare()
        {
            Console.WriteLine("Preparing Meat Pizza!");
        }

        public override void Bake()
        {
            Console.WriteLine("Baking Meat Pizza!");
        }

        public override void Box()
        {
            Console.WriteLine("Putting Meat Pizza in a box!");
        }
    }
}
using System;
namespace _180313_ACTIVIDAD
{
    public class PepperoniPizza : Pizza
    {
        public PepperoniPizza()
        {
            name = "Peperonni Pizza";
        }

        public override void Prepare()
        {
            Console.WriteLine("Preparing Pepperoni Pizza!");
        }

        public override void Bake()
        {
            Console.WriteLine("Baking Pepperoni Pizza!");
        }

        public override void Box()
        {
            Console.WriteLine("Putting Pepperoni Pizza in a box!");
        }
    }
}
﻿using System;
using System.Linq;

namespace _180129_PRACTICA
{
    class Program
    {
        static void Main(string[] args)
        {
            Name name;
            Email email;
            Phone phone;
            Date date;
            PostalCode postalCode;
            Console.Write("Ingresar Nombre: ");
            name = new Name(Console.ReadLine());
            Console.Write("Ingresar email: ");
            email = new Email(Console.ReadLine());
            Console.Write("Ingresar telefono: ");
            phone = new Phone(Console.ReadLine());
            Console.Write("Ingresar fecha de nacimiento: ");
            date = new Date(Console.ReadLine());
            Console.Write("Ingresar CP: ");
            postalCode = new PostalCode(Console.ReadLine());

            if (new Input[]{name, email, phone, date, postalCode}.Any(x => !x.Validate())){
                Console.WriteLine("\nFormulario no valido");
            }
            else {
                Console.WriteLine("\nFormulario Valido");
            }
        }
    }
}

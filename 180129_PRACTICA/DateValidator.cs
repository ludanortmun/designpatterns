using System;

namespace _180129_PRACTICA
{
    public class DateValidator : IInputValidator
    {
        public bool IsValid(string text)
        {
            DateTime date = new DateTime();
            return DateTime.TryParse(text, out date);
        }
    }
}  
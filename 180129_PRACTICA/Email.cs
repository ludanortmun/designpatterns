using System;

namespace _180129_PRACTICA
{
    public class Email : Input
    {
        public Email(string text)
        {
            this.text = text;
            validator = new EmailValidator();
        }
        public override bool Validate()
        {
            return validator.IsValid(text);
        }
    }
}
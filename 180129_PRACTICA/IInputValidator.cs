namespace _180129_PRACTICA
{
    public interface IInputValidator
    {
        bool IsValid(string text);
    }
}
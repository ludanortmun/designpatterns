namespace _180129_PRACTICA
{
    public abstract class Input
    {
        protected string text;
        protected IInputValidator validator;
        public abstract bool Validate();
    }
}
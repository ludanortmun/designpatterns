using System;

namespace _180129_PRACTICA
{
    public class PostalCode : Input
    {
        public PostalCode(string text)
        {
            this.text = text;
            validator = new PostalCodeValidator();
        }

        public override bool Validate()
        {
            return validator.IsValid(text);
        }
    }   
}
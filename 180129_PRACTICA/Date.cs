using System;

namespace _180129_PRACTICA
{
    public class Date : Input
    {
        public Date(string text)
        {
            this.text = text;
            validator = new DateValidator();
        }

        public override bool Validate()
        {
            return validator.IsValid(text);
        }
    }   
}
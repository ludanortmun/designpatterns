using System;

namespace _180129_PRACTICA
{
    public class PhoneValidator : IInputValidator
    {
        public bool IsValid(string text)
        {
            text = text.Replace("-","");
            text = text.Replace("(","");
            text = text.Replace(")","");
            text = text.Replace(" ", "");
            int test = 0;
            if (!(text.Length == 10 || text.Length == 7)) {
                return false;
            }
            return int.TryParse(text, out test);
        }
    }
}
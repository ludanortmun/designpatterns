using System;

namespace _180129_PRACTICA
{
    public class Name : Input
    {
        public Name(string text)
        {
            this.text = text;
            validator = new NameValidator();
        }
        public override bool Validate()
        {
            return validator.IsValid(text);
        }
    }
}
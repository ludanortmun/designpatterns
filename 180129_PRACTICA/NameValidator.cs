using System;
using System.Text.RegularExpressions;

namespace _180129_PRACTICA
{
    public class NameValidator : IInputValidator
    {
        public bool IsValid(string text)
        {
            string pattern = "^[A-Za-z ]+$";
            Regex regex = new Regex(pattern);
            return regex.IsMatch(text);
        }
    }
}
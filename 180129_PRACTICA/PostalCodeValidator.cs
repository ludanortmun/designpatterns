using System;

namespace _180129_PRACTICA
{
    public class PostalCodeValidator : IInputValidator
    {   
        public bool IsValid(string text)
        {
            if (text.Length != 5) {
                return false;
            }
            int temp = 0;
            return int.TryParse(text, out temp);
        }
    }   
}
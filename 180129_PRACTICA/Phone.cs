using System;

namespace _180129_PRACTICA
{
    public class Phone : Input
    {
        public Phone(string text)
        {
            this.text = text;
            validator = new PhoneValidator();
        }
        public override bool Validate()
        {
            return validator.IsValid(text);
        }
    }   
}
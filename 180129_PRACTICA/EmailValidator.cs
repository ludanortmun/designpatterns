using System;
using System.Linq;

namespace _180129_PRACTICA
{
    public class EmailValidator : IInputValidator
    {
        public bool IsValid(string text)
        {
            if (text.Count(x => x == '@') != 1) {
                return false;
            }


            if (text.Split('.').Last().Length < 2){
                return false;
            }
            
            return true;
        }
    }
}
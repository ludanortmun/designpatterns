using System;

namespace _180124_PRACTICA
{
    public abstract class Character
    {
        protected IWeaponBehavior weapon;

        public void setWeapon(IWeaponBehavior w)
        {
            weapon = w;
        }

        public abstract void fight();
    }
}
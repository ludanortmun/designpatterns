﻿using System;

namespace _180124_PRACTICA
{
    class Program
    {
        static void Main(string[] args)
        {
            Character[] characters = { new Knight(), new Troll(), new King(), new Queen() };
            IWeaponBehavior[] weapons = { new KnifeBehavior(), new SwordBehavior(), new AxeBehavior(), new BowAndArrowBehavior() };
            for (int i = 0; i < 4; i++)
            {
                Character c = characters[i];
                //First attack
                c.fight();
                //Switch weapon
                c.setWeapon(weapons[i]);
            }

            Console.WriteLine("\nRound Two!\n");

            foreach (Character c in characters)
            {
                c.fight();
            }
        }
    }
}

using System;

namespace _180124_PRACTICA
{
    public class AxeBehavior : IWeaponBehavior
    {
        public void useWeapon()
        {
            Console.WriteLine("Swinging my Axe!");
        }
    }
}
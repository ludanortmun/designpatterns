using System;

namespace _180124_PRACTICA
{
    public class Knight : Character
    {
        public Knight() 
        {
            setWeapon(new BowAndArrowBehavior());
        }
        public override void fight()
        {
            Console.Write("Knight attacks: ");
            weapon.useWeapon();
        }
    }
}
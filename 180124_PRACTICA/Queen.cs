using System;

namespace _180124_PRACTICA 
{
    public class Queen : Character 
    {
        public Queen()
        {
            setWeapon(new KnifeBehavior());
        }

        public override void fight()
        {
            Console.Write("Queen attacks: ");
            weapon.useWeapon();
        }
    }
}
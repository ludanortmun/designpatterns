using System;

namespace _180124_PRACTICA
{
    public class KnifeBehavior : IWeaponBehavior
    {
        public void useWeapon()
        {
            Console.WriteLine("Slashing with Knife!");
        }
    }
}
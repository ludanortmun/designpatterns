using System;

namespace _180124_PRACTICA
{
    public class King : Character 
    {
        public King() 
        {
            setWeapon(new SwordBehavior());
        }

        public override void fight()
        {
            Console.Write("King attacks: ");
            weapon.useWeapon();
        }
    }
}
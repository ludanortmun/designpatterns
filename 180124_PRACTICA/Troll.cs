using System;

namespace _180124_PRACTICA 
{
    public class Troll : Character
    {
        public Troll()
        {
            setWeapon(new AxeBehavior());
        }

        public override void fight()
        {
            Console.Write("Troll attacks: ");
            weapon.useWeapon();
        }
    }
}
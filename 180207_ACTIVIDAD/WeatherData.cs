using System;
using System.Collections.Generic;

namespace _180207_ACTIVIDAD
{
    public class WeatherData : ISubject
    {
        private List<IWeatherObserver> observers;
        private double temperature;
        private double humidity;
        private double pressure;
        private bool changed;
        private Random random;

        public WeatherData()
        {
            random = new Random();
            temperature = random.NextDouble();
            humidity = random.NextDouble();
            pressure = random.NextDouble();
            changed = false;

            observers = new List<IWeatherObserver>();
        }

        public void register(IWeatherObserver observer)
        {
            observers.Add(observer);
            Console.WriteLine("Registered an Observer!");
        }

        public void remove(IWeatherObserver observer)
        {
            observers.Remove(observer);
            Console.WriteLine("Removed an Observer!");
        }

        public void notify()
        {
            if (!changed) {
                Console.WriteLine("Nothing to report!");
                return;
            }
            foreach(IWeatherObserver observer in observers) {
                observer.refresh(new WeatherWrapper(temperature, humidity, pressure));
            }
            Console.WriteLine("Changes sent!");
            changed = false;
        }

        public void measurementsChanged()
        {
            temperature = random.NextDouble();
            humidity = random.NextDouble();
            pressure = random.NextDouble();
            setChanged();
            Console.WriteLine("Measurements have changed");
            notify();
        }

        private void setChanged() 
        {
            changed = true;
        }

        private void clearChanged()
        {
            changed = false;
        }
    }
}
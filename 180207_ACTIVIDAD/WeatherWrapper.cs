using System;
using System.Collections.Generic;

namespace _180207_ACTIVIDAD
{
    public class WeatherWrapper
    {
        public double Temperature {
            get;
        }
        public double Humidity {
            get;
        }
        public double Pressure {
            get;
        }
        public WeatherWrapper(double temp, double hum, double press) 
        {
            Temperature = temp;
            Humidity = hum;
            Pressure = press;
        }
    }
}
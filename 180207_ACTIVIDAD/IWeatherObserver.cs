namespace _180207_ACTIVIDAD
{
    public interface IWeatherObserver
    {
        void refresh(WeatherWrapper data);
    }
}
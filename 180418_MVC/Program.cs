﻿using System;

namespace _180418_MVC
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(HelloWorld.SayHello());

            int num1, num2;
            Console.Write("Ingresa un número: ");
            while(!int.TryParse(Console.ReadLine(), out num1)) {
                Console.Write("Número no válido. Intente de nuevo: ");
            }
            
            Console.Write("Ingresa un segundo número: ");
            while(!int.TryParse(Console.ReadLine(), out num2)) {
                Console.Write("Número no válido. Intente de nuevo: ");
            }

            Console.WriteLine("Resultado: {0}", Calculator.SumIntegers(num1, num2));
        }
    }
}

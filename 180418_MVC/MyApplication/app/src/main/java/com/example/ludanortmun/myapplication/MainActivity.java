package com.example.ludanortmun.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    Calculator calc;
    EditText et_a, et_b;
    TextView tv_resultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        calc = new Calculator();
        setContentView(R.layout.activity_main);
        et_a = findViewById(R.id.num1);
        et_b = findViewById(R.id.num2);
        tv_resultado = findViewById(R.id.result);
    }

    public void onButtonClick(View view) {

        try {
            double a = Double.parseDouble(et_a.getText().toString());
            double b = Double.parseDouble(et_b.getText().toString());
            double result = 0;
            switch (view.getId()) {
                case R.id.buttonSum:
                    result = calc.sum(a, b);
                    break;
                case R.id.buttonSub:
                    result = calc.sub(a, b);
                    break;
                case R.id.buttonMult:
                    result = calc.mult(a, b);
                    break;
                case R.id.buttonDiv:
                    result = calc.div(a, b);
                    break;
            }
            tv_resultado.setText(result + "");
        } catch (IllegalArgumentException e) {
            tv_resultado.setText("ERROR");
        }
    }
}

namespace _180313_PRACTICA
{
    public class Bacon : Ingredient
    {
        public Bacon(Pizza parent)
        {
            this.name = "Bacon";
            this.parent = parent;
            this.price = 1.0;
        }

        public override double getPrice() 
        {
            return parent.getPrice() + price;
        }
    }
}
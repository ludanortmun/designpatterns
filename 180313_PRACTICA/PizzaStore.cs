namespace _180313_PRACTICA
{
    public abstract class PizzaStore
    {
        /// <summary>
        /// <para> Prepares, bakes and boxes the final pizza, with all of the ingredients </para>
        /// </summary>
        public Pizza Order(Pizza p)
        {
            p.Prepare();
            p.Bake();
            p.Box();
            return p;
        }

        /// <summary>
        /// <para>Select an ingredient and add it to the pizza.</para>
        /// <br></br>
        /// <para>If the ingredient is not available in that factory, 
        /// an ArgumentException is raised</para>
        /// </summary>
        public abstract Pizza SelectIngredient(string ing, Pizza p);

        /// <summary>
        /// Creates the default pizza for the factory of the specified size.
        /// </summary>
        public abstract Pizza CreatePizza(string size);
    }
}
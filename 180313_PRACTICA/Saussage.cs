namespace _180313_PRACTICA
{
    public class Saussage : Ingredient
    {
        public Saussage(Pizza parent)
        {
            this.name = "Saussage";
            this.parent = parent;
            this.price = 1.0;
        }

        public override double getPrice() 
        {
            return parent.getPrice() + price;
        }
    }
}
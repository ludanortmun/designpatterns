namespace _180313_PRACTICA
{
    public class Almond : Ingredient
    {
        public Almond(Pizza parent)
        {
            this.name = "Almond";
            this.parent = parent;
            this.price = 2.0;
        }

        public override double getPrice() 
        {
            return parent.getPrice() + price;
        }
    }
}
using System;

namespace _180313_PRACTICA
{
    public class TraditionalPizzaStore : PizzaStore
    {
        public override Pizza SelectIngredient(string ing, Pizza p)
        {
            switch (ing) {
                case "Pepperoni":
                    return new Pepperoni(p);
                case "Saussage":
                    return new Saussage(p);
                case "Cheese Crust":
                    return new CheeseCrust(p);
                default:
                    throw new ArgumentException();
            }
        }

        public override Pizza CreatePizza(string size)
        {
            return new TraditionalPizza(size);
        }
    }
}
namespace _180313_PRACTICA
{
    public class Pepperoni : Ingredient
    {
        public Pepperoni(Pizza parent)
        {
            this.name = "Pepperoni";
            this.parent = parent;
            this.price = 1.0;
        }

        public override double getPrice() 
        {
            return parent.getPrice() + price;
        }
    }
}
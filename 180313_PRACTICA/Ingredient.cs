using System;
namespace _180313_PRACTICA
{
    public abstract class Ingredient : Pizza
    {
        internal Pizza parent;

        public override Pizza getParent()
        {
            return parent.getParent();
        }

        //Abstracto para que cada clase concreta calcule si precio como sea necesario
        public override abstract double getPrice();
        public override string getSize()
        {
            return parent.getSize();
        }

        public override string getName()
        {
            return string.Format("{0}; {1}", parent.getName(), name);
        }

        public override void Prepare()
        {
            Console.WriteLine("Preparing {0}", getName());
        }

        public override void Bake()
        {
            Console.WriteLine("Baking {0}", getName());
        }

        public override void Box()
        {
            Console.WriteLine("Boxing {0}", getName());
        }
    }
}
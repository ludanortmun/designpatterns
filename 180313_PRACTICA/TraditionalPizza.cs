using System;
namespace _180313_PRACTICA
{
    public class TraditionalPizza : Pizza
    {
        public TraditionalPizza(string size)
        {
            price = 3;
            this.size = size;
            name = "Traditional Pizza";
        }
        public override void Prepare()
        {
            Console.WriteLine("Preparing {0}", getName());
        }

        public override void Bake()
        {
            Console.WriteLine("Baking {0}", getName());
        }

        public override void Box()
        {
            Console.WriteLine("Boxing {0}", getName());
        }

        public override double getPrice() 
        {
            switch (getSize()) {
                case "L":
                    return price * 2;
                case "M":
                    return price * 1.5;
                case "S":
                    return price;
                default:
                    throw new Exception();
            }
        }
    }
}
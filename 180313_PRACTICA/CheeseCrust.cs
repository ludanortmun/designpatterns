namespace _180313_PRACTICA
{
    public class CheeseCrust : Ingredient
    {
        public CheeseCrust(Pizza parent)
        {
            this.name = "Cheese Crust";
            this.parent = parent;
            this.price = 1.0;
        }

        //Depends on size
        public override double getPrice() 
        {
            double newPrice = 0;
            switch (parent.getSize()) {
                case "S":
                    newPrice = price;
                    break;
                case "M":
                    newPrice = price * 1.5;
                    break;
                case "L":
                    newPrice = price * 2;
                    break;
            }
            return parent.getPrice() + newPrice;
        }
    }
}
using System;

namespace _180313_PRACTICA
{
    public class HipsterPizzaStore : PizzaStore
    {
        public override Pizza SelectIngredient(string ing, Pizza p)
        {
            switch (ing) {
                case "Vegan Bacon":
                    return new VeganBacon(p);
                case "Almond":
                    return new Almond(p);
                case "Vegan Cheese Crust":
                    return new VeganCheeseCrust(p);
                default:
                    throw new ArgumentException();
            }
        }

        public override Pizza CreatePizza(string size)
        {
            return new HipsterPizza(size);
        }
    }
}
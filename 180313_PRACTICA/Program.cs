﻿using System;

namespace _180313_PRACTICA
{
    class Program
    {
        static void Main(string[] args)
        {
            PizzaStore store = new TraditionalPizzaStore();
            Pizza p = store.CreatePizza("S");
            Console.WriteLine("{0} - Total: ${1:0.0}", p.getName(), p.getPrice());
            p = store.SelectIngredient("Pepperoni", p);
            Console.WriteLine("{0} - Total: ${1:0.0}", p.getName(), p.getPrice());
            p = store.SelectIngredient("Saussage", p);
            Console.WriteLine("{0} - Total: ${1:0.0}", p.getName(), p.getPrice());
            p = store.SelectIngredient("Cheese Crust", p);
            Console.WriteLine("{0} - Total: ${1:0.0}", p.getName(), p.getPrice());
            try {
                p = store.SelectIngredient("Almond", p);
                Console.WriteLine("{0} - Total: ${1:0.0}", p.getName(), p.getPrice());
            } catch (ArgumentException e) {
                Console.WriteLine("That ingredient is not available here!");
            }
            store.Order(p);

            Console.WriteLine();

            p = store.CreatePizza("L");
            Console.WriteLine("{0} - Total: ${1:0.0}", p.getName(), p.getPrice());
            p = store.SelectIngredient("Pepperoni", p);
            Console.WriteLine("{0} - Total: ${1:0.0}", p.getName(), p.getPrice());
            p = store.SelectIngredient("Saussage", p);
            Console.WriteLine("{0} - Total: ${1:0.0}", p.getName(), p.getPrice());
            p = store.SelectIngredient("Cheese Crust", p);
            Console.WriteLine("{0} - Total: ${1:0.0}", p.getName(), p.getPrice());

            Console.WriteLine("\nEntering Hipster Pizza Store\n");
            store = new HipsterPizzaStore();
            p = store.CreatePizza("M");
            p = store.SelectIngredient("Almond", p);
            Console.WriteLine("{0} - Total: ${1:0.0}", p.getName(), p.getPrice());
            p = store.SelectIngredient("Vegan Bacon", p);
            Console.WriteLine("{0} - Total: ${1:0.0}", p.getName(), p.getPrice());
            p = store.SelectIngredient("Vegan Bacon", p);
            Console.WriteLine("{0} - Total: ${1:0.0}", p.getName(), p.getPrice());
            try {
                p = store.SelectIngredient("Saussage", p);
                Console.WriteLine("{0} - Total: ${1:0.0}", p.getName(), p.getPrice());
            } catch (ArgumentException e) {
                Console.WriteLine("That ingredient is not available here!");
            }
            store.Order(p);
        }
    }
}

namespace _180313_PRACTICA
{
    public abstract class Pizza
    {
        internal string name;
        internal double price;
        internal string size;

        public abstract void Prepare();
        public abstract void Bake();
        public abstract void Box();

        public virtual Pizza getParent()
        {
            return this;
        }

        public virtual double getPrice()
        {
            return price;
        }

        public virtual string getName()
        {
            return size + " " + name;
        }

        public virtual string getSize()
        {
            return size;
        }
    }
}
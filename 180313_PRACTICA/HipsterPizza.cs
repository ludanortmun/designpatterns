using System;
namespace _180313_PRACTICA
{
    public class HipsterPizza : Pizza
    {
        public HipsterPizza(string size)
        {
            this.size = size;
            this.name = "Hipster Pizza";
            this.price = 7;
        }
        public override void Prepare()
        {
            Console.WriteLine("Preparing {0}", getName());
        }

        public override void Bake()
        {
            Console.WriteLine("Baking {0}", getName());
        }

        public override void Box()
        {
            Console.WriteLine("Boxing {0}", getName());
        }

         public override double getPrice() 
        {
            switch (getSize()) {
                case "L":
                    return price * 3;
                case "M":
                    return price * 2;
                case "S":
                    return price;
                default:
                    throw new Exception();
            }
        }
    }
}
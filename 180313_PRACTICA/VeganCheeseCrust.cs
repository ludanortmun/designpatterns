namespace _180313_PRACTICA
{
    public class VeganCheeseCrust : Ingredient
    {
        public VeganCheeseCrust(Pizza parent)
        {
            this.name = "Vegan Cheese Crust";
            this.parent = parent;
            this.price = 4.0;
        }

        public override double getPrice() 
        {
            double newPrice = 0;
            switch (parent.getSize()) {
                case "S":
                    newPrice = price;
                    break;
                case "M":
                    newPrice = price * 1.5;
                    break;
                case "L":
                    newPrice = price * 2;
                    break;
            }
            return parent.getPrice() + newPrice;
        }
    }
}
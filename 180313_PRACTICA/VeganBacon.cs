namespace _180313_PRACTICA
{
    public class VeganBacon : Ingredient
    {
        public VeganBacon(Pizza parent)
        {
            this.name = "Vegan Bacon";
            this.parent = parent;
            this.price = 3.0;
        }

        public override double getPrice() 
        {
            return parent.getPrice() + price;
        }
    }
}
using System;
using System.Collections.Generic;

namespace _180206_ACTIVIDAD
{
    public class HumidityDisplay : IDisplayable, IWeatherObserver
    {
        private double humidity;
        private bool gotData;
        
        public HumidityDisplay()
        {
            gotData = false;
        }

        public void refresh(WeatherWrapper data) 
        {
            humidity = data.Humidity;
            gotData = true;
            show();
        }

        public void show()
        {
            if (gotData) {
                Console.WriteLine("Humidity is {0}", humidity);
            }
            else {
                Console.WriteLine("I don't have any humidity data!");
            }
        }

    }
}
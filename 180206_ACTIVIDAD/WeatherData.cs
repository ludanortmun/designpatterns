using System;
using System.Collections.Generic;

namespace _180206_ACTIVIDAD
{
    public class WeatherData : ISubject
    {
        private List<IWeatherObserver> observers;
        private double temperature;
        private double humidity;
        private double pressure;
        private Random random;

        public WeatherData()
        {
            random = new Random();
            temperature = random.NextDouble();
            humidity = random.NextDouble();
            pressure = random.NextDouble();

            observers = new List<IWeatherObserver>();
        }

        public void register(IWeatherObserver observer)
        {
            observers.Add(observer);
            Console.WriteLine("Registered an Observer!");
        }

        public void remove(IWeatherObserver observer)
        {
            observers.Remove(observer);
            Console.WriteLine("Removed an Observer!");
        }

        public void notify()
        {
            foreach(IWeatherObserver observer in observers) {
                observer.refresh(new WeatherWrapper(temperature, humidity, pressure));
            }
        }

        public void measurementsChanged()
        {
            temperature = random.NextDouble();
            humidity = random.NextDouble();
            pressure = random.NextDouble();
            Console.WriteLine("Measurements have changed");
            notify();
        }
    }
}
namespace _180206_ACTIVIDAD
{
    public interface IWeatherObserver
    {
        void refresh(WeatherWrapper data);
    }
}
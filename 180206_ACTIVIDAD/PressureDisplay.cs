using System;
using System.Collections.Generic;

namespace _180206_ACTIVIDAD
{
    public class PressureDisplay : IDisplayable, IWeatherObserver
    {
        private double pressure;
        private bool gotData;

        public PressureDisplay()
        {
            gotData = false;
        }

        public void refresh(WeatherWrapper data) 
        {
            pressure = data.Pressure;
            gotData = true;
            show();
        }

        public void show()
        {
            if (gotData) {
                Console.WriteLine("Pressure is {0}", pressure);
            }
            else {
                Console.WriteLine("I don't have any pressure data!");
            }
        }

    }
}
﻿using System;

namespace _180206_ACTIVIDAD
{
    class Program
    {
        static void Main(string[] args)
        {
            WeatherData weatherData = new WeatherData();
            TemperatureDisplay temp = new TemperatureDisplay();
            HumidityDisplay hum = new HumidityDisplay();
            PressureDisplay press = new PressureDisplay();

            weatherData.register(temp);
            temp.show();
            weatherData.register(hum);
            hum.show();
            press.show();

            weatherData.notify();
            weatherData.register(press);

            weatherData.measurementsChanged();

            weatherData.remove(temp);
            weatherData.measurementsChanged();
        }
    }
}

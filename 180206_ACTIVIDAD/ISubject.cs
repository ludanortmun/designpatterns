using System;
using System.Collections.Generic;

namespace _180206_ACTIVIDAD
{
    public interface ISubject
    {
        void register(IWeatherObserver observer);
        void remove(IWeatherObserver observer);
        void notify();
    }
}
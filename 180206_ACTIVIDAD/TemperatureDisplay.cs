using System;
using System.Collections.Generic;

namespace _180206_ACTIVIDAD
{
    public class TemperatureDisplay : IDisplayable, IWeatherObserver
    {
        private double temperature;
        private bool gotData;

        public TemperatureDisplay()
        {
            gotData = false;
        }

        public void refresh(WeatherWrapper data) 
        {
            temperature = data.Temperature;
            gotData = true;
            show();
        }

        public void show()
        {
            if (gotData) {
                Console.WriteLine("Temperature is {0}", temperature);
            }
            else {
                Console.WriteLine("I don't have any temperature data!");
            }
        }

    }
}
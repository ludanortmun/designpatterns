namespace _180320_ACTIVIDAD
{
    public class DimLightCommand : ICommand
    {
        public Light light;

        public DimLightCommand(Light light)
        {
            this.light = light;
        }

        public void execute()
        {
            light.dim();
        }

        public void undo()
        {
            light.dim();
        }
    }
}
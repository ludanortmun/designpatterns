﻿using System;
using System.Collections.Generic;

namespace _180320_ACTIVIDAD
{
    class Program
    {
        static void Main(string[] args)
        {
            Door door = new Door("Front Door");
            Light light = new Light("Living room Light");
            CeilingFan fan = new CeilingFan("Living room fan");
            Invoker invoker = new Invoker();
            List<ICommand> gettingHome = new List<ICommand>();
            gettingHome.Add(new LockCommand(door));
            gettingHome.Add(new OpenDoorCommand(door));
            gettingHome.Add(new ToggleLightCommand(light));
            gettingHome.Add(new CloseDoorCommand(door));
            gettingHome.Add(new LockCommand(door));

            List<ICommand> chilling = new List<ICommand>();
            chilling.Add(new CeilingFanCommand(fan));
            //Should replace this with turning on radio or TV
            chilling.Add(new CeilingFanCommand(fan));
            chilling.Add(new CeilingFanCommand(fan));

            Routine chillingRoutine = new Routine(chilling);
            Routine gettingHomeRoutine = new Routine(gettingHome);

            invoker.setCommand(new CloseDoorCommand(door), 0);
            invoker.setCommand(new DimLightCommand(light), 1);
            invoker.setCommand(new LockCommand(door), 2);
            invoker.setCommand(new OpenDoorCommand(door), 3);
            invoker.setCommand(new ToggleLightCommand(light), 4);
            invoker.setCommand(new CeilingFanCommand(fan), 5);
            invoker.setCommand(new Routine(new Routine[] { gettingHomeRoutine, chillingRoutine }) , 6);

            // for (int i = 0; i < 5; i++) {
            //     invoker.runCommand(5);
            // }
            // for (int i = 0; i < 5; i++) {
            //     invoker.undoCommand();
            // }

            invoker.runCommand(6);
            invoker.undoCommand();
        }
    }
}

namespace _180320_ACTIVIDAD
{
    public class CloseDoorCommand : ICommand
    {
        public Door door;

        public CloseDoorCommand(Door door)
        {
            this.door = door;
        }

        public void execute()
        {
            door.close();
        }

        public void undo()
        {
            door.open();
        }
    }
}
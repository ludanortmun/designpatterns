using System;

namespace _180320_ACTIVIDAD
{
    public class NullCommand : ICommand
    {
        public void execute()
        {
            Console.WriteLine("No command");
        }

        public void undo()
        {
            Console.WriteLine("No command");
        }
    }
}
using System;
using System.Collections.Generic;
using System.Linq;

namespace _180320_ACTIVIDAD
{
    public class Routine : ICommand
    {
        private IEnumerable<ICommand> commands;

        public Routine(IEnumerable<ICommand> commands)
        {
            this.commands = commands;
        }

        public void execute()
        {
            foreach(ICommand cmd in commands) {
                cmd.execute();
            }
        }

        public void undo() 
        {
            foreach(ICommand cmd in commands.Reverse()) {
                cmd.undo();
            }
        }
    }
}
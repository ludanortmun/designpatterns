namespace _180320_ACTIVIDAD
{
    public class OpenDoorCommand : ICommand
    {
        public Door door;

        public OpenDoorCommand(Door door)
        {
            this.door = door;
        }

        public void execute()
        {
            door.open();
        }

        public void undo()
        {
            door.close();
        }
    }
}
using System;

namespace _180320_ACTIVIDAD
{
    public class Invoker
    {
        private ICommand[] commands;
        private ICommand lastCmd;

        public Invoker()
        {
            //Change size according to specifications
            commands = new ICommand[8];
            for (int i = 0; i < 8; i++) {
                commands[i] = new NullCommand();
            }
        }

        public void setCommand(ICommand command, int i) 
        {
            commands[i] = command;
        }

        public void runCommand(int i)
        {
            commands[i].execute();
            //Only change last command if the command itself is set.
            if (commands[i].GetType() != typeof(NullCommand)) {
                lastCmd = commands[i];
            }
        }
        
        public void undoCommand()
        {
            lastCmd.undo();
        }
    }
}
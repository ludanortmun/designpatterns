namespace _180320_ACTIVIDAD
{
    public class LockCommand : ICommand
    {
        public Door door;

        public LockCommand(Door door) 
        {
            this.door = door;
        }

        public void execute()
        {
            door.toggleLock();
        }

        public void undo()
        {
            door.toggleLock();
        }
    }
}
using System;

public class Door
{
    public string description;
    private bool isOpen;
    private bool isLocked;

    public Door(string description)
    {
        this.description = description;
        isOpen = false;
        isLocked = true;
    }

    public void toggleLock()
    {
        if (isLocked) {
            Console.WriteLine("Unlocking door");
        } else {
            Console.WriteLine("Locking door");
        }
        isLocked = !isLocked;
    }

    public void open()
    {
        if (isOpen) {
            Console.WriteLine("Door is already open");
        } else {
            Console.WriteLine("Opening door");
            isOpen = true;
        }
    }

    public void close()
    {
        if (!isOpen) {
            Console.WriteLine("Door is already closed");
        } else {
            Console.WriteLine("Closing door");
            isOpen = false;
        }
    }
}
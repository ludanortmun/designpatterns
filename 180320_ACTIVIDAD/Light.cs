using System;

namespace _180320_ACTIVIDAD
{
    public class Light
    {
        public string descrition;
        private bool isOn;
        private bool isDimmed;

        public Light(string descrition) 
        {
            this.descrition = descrition;
            isOn = false;
            isDimmed = false;
        }

        public void toggle()
        {
            if (isOn) {
                Console.WriteLine("Shutting down light");
            } else {
                Console.WriteLine("Turning on light");
            }
            isOn = !isOn;
        }

        public void dim()
        {
            if (isDimmed) {
                Console.WriteLine("Undimming light");
            } else{
                Console.WriteLine("Dimming light");
            }
            isDimmed = !isDimmed;
        }
    }
}
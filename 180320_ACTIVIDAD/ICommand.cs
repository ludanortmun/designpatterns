namespace _180320_ACTIVIDAD
{
    public interface ICommand
    {
        void execute();
        void undo();
    }
}
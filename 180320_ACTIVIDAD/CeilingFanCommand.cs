namespace _180320_ACTIVIDAD
{
    public class CeilingFanCommand : ICommand
    {
        public CeilingFan ceilingFan;

        public CeilingFanCommand(CeilingFan ceilingFan)
        {
            this.ceilingFan = ceilingFan;
        }

        public void execute()
        {
            switch (ceilingFan.GetSpeed()) {
                case FanSpeeds.OFF:
                    ceilingFan.low();
                    break;
                case FanSpeeds.LOW:
                    ceilingFan.medium();
                    break;
                case FanSpeeds.MEDIUM:
                    ceilingFan.high();
                    break;
                case FanSpeeds.HIGH:
                    ceilingFan.off();
                    break;
            }
        }

        public void undo()
        {
            switch(ceilingFan.GetSpeed()) {
                case FanSpeeds.OFF:
                    ceilingFan.off();
                    break;
                case FanSpeeds.LOW:
                    ceilingFan.off();
                    break;
                case FanSpeeds.MEDIUM:
                    ceilingFan.low();
                    break;
                case FanSpeeds.HIGH:
                    ceilingFan.medium();
                    break;
            }
        }
    }
}
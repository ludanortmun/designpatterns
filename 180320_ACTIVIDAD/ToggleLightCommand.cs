namespace _180320_ACTIVIDAD
{
    public class ToggleLightCommand : ICommand
    {
        public Light light;

        public ToggleLightCommand(Light light)
        {
            this.light = light;
        }

        public void execute()
        {
            light.toggle();
        }

        public void undo()
        {
            light.toggle();
        }
    }
}
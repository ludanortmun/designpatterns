using System;

namespace _180320_ACTIVIDAD
{
    public class CeilingFan
    {
        public string description;
        private FanSpeeds speed;

        public CeilingFan(string description)
        {
            this.description = description;
            speed = FanSpeeds.OFF;
        }

        public void high()
        {
            Console.WriteLine("High speed");
            speed = FanSpeeds.HIGH;
        }

        public void medium()
        {
            Console.WriteLine("Medium speed");
            speed = FanSpeeds.MEDIUM;
        }

        public void low()
        {
            Console.WriteLine("Low speed");
            speed = FanSpeeds.LOW;
        }

        public void off()
        {
            Console.WriteLine("Off");
            speed = FanSpeeds.OFF;
        }

        public FanSpeeds GetSpeed()
        {
            return speed;
        }
    }
}
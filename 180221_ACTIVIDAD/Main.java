import java.io.*;

public class Main
{
    public static void main(String[] args) {
        try {
            InputStream in = new FileInputStream("test.txt");
            in = new LowerInputStream(in);
            int c;
            while ((c = in.read()) >= 0) {
                System.out.print((char)c);
            }
            System.out.println();
            in.close();
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
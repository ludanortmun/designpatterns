import java.io.*;

public class LowerInputStream extends FilterInputStream
{
    public LowerInputStream(InputStream in) 
    {
        super(in);
    }

    public int read() throws IOException
    {
        int result = 0;
        result = in.read();
        if (result == -1) {
            return -1;
        }
        result = Character.toLowerCase((char)result);
        return result;
    }
}
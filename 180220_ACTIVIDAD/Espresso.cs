namespace _180220_ACTIVIDAD
{
    public class Espresso : Ingredient
    {
        public Espresso(Beverage parent)
        {
            this.parent = parent;
            this.Description = "Espresso";
            this.Cost = 2;
        }
    }
}
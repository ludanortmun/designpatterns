﻿using System;

namespace _180220_ACTIVIDAD
{
    class Program
    {
        static void Main(string[] args)
        {
            Beverage b = new Mocha("L");
            b = new Espresso(b);
            b = new Milk(b);
            Console.WriteLine("{0} - {1}",b.getDescription(), b.getCost());

            b = new Coffee("L");
            b = new Milk(b);
            b = new Cream(b);
            Console.WriteLine("{0} - {1}",b.getDescription(), b.getCost());

        }
    }
}

using System;

namespace _180220_ACTIVIDAD
{
    public class Coffee : Beverage
    {
        public Coffee(string s)
        {
            Description = "Coffee";
            if (s == "S") {
                Cost = 5;
            }
            else if (s == "M") {
                Cost = 10;
            }
            else if (s == "L") {
                Cost = 15;
            }
            else {
                throw new NotImplementedException();
            }
            Size = s;
        }

        public override string getDescription()
        {
            return Size + " " + Description + " ";
        }

        public override double getCost()
        {
            return Cost;
        }

        public override string getSize()
        {
            return Size;
        }
    }
}
using System;

namespace _180220_ACTIVIDAD
{
    public class Mocha : Beverage
    {
        public Mocha(string s)
        {
            Description = "Mocha";
            if (s == "S") {
                Cost = 10;
            }
            else if (s == "M") {
                Cost = 15;
            }
            else if (s == "L") {
                Cost = 20;
            }
            else {
                throw new NotImplementedException();
            }
            Size = s;
        }

        public override string getDescription()
        {
            return Size + " " + Description + " ";
        }

        public override double getCost()
        {
            return Cost;
        }

        public override string getSize()
        {
            return Size;
        }
    }
}
using System;

namespace _180220_ACTIVIDAD
{
    public class Cappuccino : Beverage
    {
        public Cappuccino(string s)
        {
            Description = "Cappuccino";
            if (s == "S") {
                Cost = 9;
            }
            else if (s == "M") {
                Cost = 16;
            }
            else if (s == "L") {
                Cost = 25;
            }
            else {
                throw new NotImplementedException();
            }
            Size = s;
        }

        public override string getDescription()
        {
            return Size + " " + Description + " ";
        }

        public override double getCost()
        {
            return Cost;
        }

        public override string getSize()
        {
            return Size;
        }
    }
}
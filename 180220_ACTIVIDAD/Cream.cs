namespace _180220_ACTIVIDAD
{
    public class Cream : Ingredient
    {
        public Cream(Beverage parent)
        {
            this.parent = parent;
            this.Description = "Cream";
            this.Cost = 5;
        }
    }
}
namespace _180220_ACTIVIDAD
{
    public abstract class Beverage
    {
        internal string Description;
        internal string Size;
        internal double Cost;
        public abstract string getDescription();
        public abstract double getCost();
        public abstract string getSize();
    }
}
namespace _180220_ACTIVIDAD
{
    public class Milk : Ingredient
    {
        public Milk(Beverage parent)
        {
            this.parent = parent;
            this.Description = "Milk";
            if (parent.getSize() == "S") {
                this.Cost = 0.5;
            }
            else if (parent.getSize() == "M") {
                this.Cost = 1;
            }
            else if (parent.getSize() == "L") {
                this.Cost = 2;
            }
        }
    }
}
namespace _180220_ACTIVIDAD
{
    public abstract class Ingredient : Beverage
    {
        internal Beverage parent;
        public override double getCost()
        {
            return parent.getCost() + Cost;
        }
        public override string getDescription()
        {
            return parent.getDescription() + "with " + this.Description + " ";
        }

        public override string getSize()
        {
            return parent.getSize();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Linq;

namespace _180213_PRACTICA
{
    class Program
    {
        static void Main(string[] args)
        {
            DHL dhl = new DHL();
            Fedex fedex = new Fedex();
            Sepomex sepomex = new Sepomex();

            Thread t_dhl = new Thread(dhl.run);
            Thread t_fedex = new Thread(fedex.run);
            Thread t_sepomex = new Thread(sepomex.run);
            t_dhl.Start();
            t_fedex.Start();
            t_sepomex.Start();
            

            Client cliente1 = new Client("Client 1");
            Client cliente2 = new Client("Client 2");

            //Listas para utilizarse sólamente en la demostración para almacenar los guid
            List<Tuple<Guid, string>> client1Packages = new List<Tuple<Guid, string>>();
            List<Tuple<Guid, string>> client2Packages = new List<Tuple<Guid, string>>();
            //Crear 2 paquetes de cada compañía en cada cliente
            for (int i = 0; i < 2; i++) {
                client1Packages.Add(new Tuple<Guid, string>(dhl.createPackage(), "DHL"));
                client1Packages.Add(new Tuple<Guid, string>(fedex.createPackage(), "Fedex"));
                client1Packages.Add(new Tuple<Guid, string>(sepomex.createPackage(), "Sepomex"));
                
                client2Packages.Add(new Tuple<Guid, string>(dhl.createPackage(), "DHL"));
                client2Packages.Add(new Tuple<Guid, string>(fedex.createPackage(), "Fedex"));
                client2Packages.Add(new Tuple<Guid, string>(sepomex.createPackage(), "Sepomex"));
            }
            foreach (Tuple<Guid, string> package in client1Packages) {
                switch (package.Item2) {
                    case "DHL":
                    cliente1.TrackPackage(package.Item1, "DHL", dhl);
                    break;
                    case "Fedex":
                    cliente1.TrackPackage(package.Item1, "Fedex", fedex);
                    break;
                    case "Sepomex":
                    cliente1.TrackPackage(package.Item1, "Sepomex", sepomex);
                    break;
                }
            }
            foreach (Tuple<Guid, string> package in client2Packages) {
                switch (package.Item2) {
                    case "DHL":
                    cliente2.TrackPackage(package.Item1, "DHL", dhl);
                    break;
                    case "Fedex":
                    cliente2.TrackPackage(package.Item1, "Fedex", fedex);
                    break;
                    case "Sepomex":
                    cliente2.TrackPackage(package.Item1, "Sepomex", sepomex);
                    break;
                }
            }

            //Eliminar packages todos los packages de un proveedor para un cliente
            Thread.Sleep(10000);
            foreach(Tuple<Guid, string> package in client1Packages.Where(x => x.Item2.Equals("DHL"))) {
                cliente1.UntrackPackage(package.Item1, "DHL", dhl);
            }

            Thread.Sleep(21000);
            //Eliminar todos los trackings de un cliente
            foreach(Tuple<Guid, string> package in client2Packages) {
                switch (package.Item2) {
                    case "DHL":
                    cliente2.UntrackPackage(package.Item1, "DHL", dhl);
                    break;
                    case "Fedex":
                    cliente2.UntrackPackage(package.Item1, "Fedex", fedex);
                    break;
                    case "Sepomex":
                    cliente2.UntrackPackage(package.Item1, "Sepomex", sepomex);
                    break;
                }
            }

            Thread.Sleep(15000);
            //Terminar el programa
            Environment.Exit(0);
        }
    }
}

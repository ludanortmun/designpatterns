using System;
using System.Collections.Generic;
using System.Threading;

namespace _180213_PRACTICA
{
    public class Sepomex : IMailingService
    {
        private const int refreshInterval = 30000;
        public const string Name = "Sepomex";
        private List<IObserver> clients;
        private volatile List<Guid> packagesIds;

        public Sepomex()
        {
            clients = new List<IObserver>();
            packagesIds = new List<Guid>();
        }

        public void register(IObserver observer)
        {
            clients.Add(observer);
        }

        public void remove(IObserver observer)
        {
            clients.Remove(observer);
        }

        public void update()
        {
            List<PackageStatus> packages = new List<PackageStatus>();
            foreach(Guid id in packagesIds) {
                packages.Add(new PackageStatus(id, Name));
            }
            foreach(IObserver observer in clients) {
                observer.refresh(packages);
            }
        }

        public void run()
        {
            while(true) {
                Thread.Sleep(refreshInterval);
                update();
            }
        }

        public Guid createPackage()
        {
            Guid guid = Guid.NewGuid();
            packagesIds.Add(guid);
            return guid;
        }
    }
}
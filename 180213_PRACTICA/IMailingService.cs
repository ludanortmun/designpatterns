using System;

namespace _180213_PRACTICA
{
    public interface IMailingService
    {
        void register(IObserver observer);
        void remove(IObserver observer);
        Guid createPackage();
        void update();
    }
}
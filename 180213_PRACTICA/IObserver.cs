using System.Collections.Generic;

namespace _180213_PRACTICA
{
    public interface IObserver
    {
        void refresh(List<PackageStatus> packages);
    }
}
using System;
using System.Collections.Generic;
using System.Linq;

namespace _180213_PRACTICA
{
    public class Client : IObserver
    {
        private volatile List<Tuple<Guid, string>> trackedPackages;
        private string name;

        public Client(string name)
        {
            trackedPackages = new List<Tuple<Guid, string>>();
            this.name = name;
        }

        public void TrackPackage(Guid id, string providerName, IMailingService provider)
        {
            if (!trackedPackages.Exists(x => x.Item2.Equals(providerName))) {
                provider.register(this);
                Console.WriteLine("{1}: Registered to provider {0}", providerName, name);
            }
            trackedPackages.Add(new Tuple<Guid, string>(id, providerName));
            Console.WriteLine("{2}: Tracking {0} package {1}", providerName, id, name);
        }

        public void UntrackPackage(Guid id, string providerName, IMailingService provider)
        {
            trackedPackages.RemoveAll(x => x.Item1 == id);
            Console.WriteLine("{2}: {0} package {1} untracked", providerName, id, name);
            if (!trackedPackages.Exists(x => x.Item2.Equals(providerName))) {
                provider.remove(this);
                Console.WriteLine("{1}: Unregistered from provider {0}", providerName, name);
            }
        }
        public void refresh(List<PackageStatus> packages)
        {
            string providerName = packages.First().Provider;
            foreach(Tuple<Guid, string> package in trackedPackages.Where(x => x.Item2.Equals(providerName))) {
                Console.WriteLine(name + ": " + packages.First(x => x.Id == package.Item1).Status);
            }
        }
    }
}
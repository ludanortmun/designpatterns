using System;

namespace _180213_PRACTICA
{
    public class PackageStatus
    {
        public Guid Id {
            get;
        }
        public string Provider {
            get;
        }
        public DateTime LastRefresh {
            get;
        }
        public string Status {
            get;
        }

        public PackageStatus(Guid id, string provider)
        {
            Id = id;
            Provider = provider;
            LastRefresh = DateTime.Now;
            Status = string.Format("Status de paquete {0} de {1}. Last Update: {2:yyyy-MM-dd HH:mm:ss}", Id, Provider, LastRefresh);
        }
    }
}
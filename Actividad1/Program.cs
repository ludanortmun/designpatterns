﻿using System;

namespace Actividad1
{
    class Program
    {
        static void Main(string[] args)
        {
            Cocinero cocinero = new Cocinero();
            cocinero.Motricidad = new HabilidadMotrizPies();
            Console.WriteLine("Cocinero con motricidad completa");
            cocinero.Motricidad.Caminar();

            Console.WriteLine("######");

            Handicapped handicapped = new Handicapped();
            handicapped.Motricidad = new HabilidadMotrizManos();
            Console.WriteLine("Handicapped con motricidad en manos");
            handicapped.Motricidad.Caminar();

            Console.WriteLine("######");

            handicapped.Motricidad = new HabilidadMotrizNula();
            Console.WriteLine("El handicapped sufre un accidente y pierde motricidad total");
            handicapped.Motricidad.Caminar();
        }
    }
}

using System;

namespace Actividad1
{
    class Handicapped : Persona
    {
        //False si puede mover brazos y no piernas, true en caso de que no pueda mover ninguno
        private bool brazos;

        public void setBrazos(bool brazos) {
            this.brazos = brazos;
        }

        public bool getBrazos() {
            return brazos;
        }
    }
}
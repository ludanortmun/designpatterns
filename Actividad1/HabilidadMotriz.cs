using System;

namespace Actividad1
{
    public class HabilidadMotrizPies : IMotricidad
    {
        public void Caminar()
        {
            Console.WriteLine("Camino con los pies");
        }
    }

    public class HabilidadMotrizManos : IMotricidad
    {
        public void Caminar()
        {
            Console.WriteLine("Camino con las manos");
        }
    }

    public class HabilidadMotrizNula: IMotricidad
    {
        public void Caminar()
        {
            Console.WriteLine("No sé caminar");
        }
    }
}
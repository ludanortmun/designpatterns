using System;

namespace Actividad1
{
    class Persona
    {
        private string nombre;
        private int edad;
        private bool genero;

        public IMotricidad Motricidad;
        
        public string getNombre()
        {
            return nombre;
        }
        public void setNombre(string nombre){
            this.nombre = nombre;
        }

        public int getEdad(){
            return edad;
        }
        public void setEdad(int edad){
            this.edad = edad;
        }

        public bool getGenero(){
            return genero;
        }
        public void setGenero(bool genero) {
            this.genero = genero;
        }
        
        public string Hablar(string msg){
            return "Acabo de decir: " + msg;
        }

        public void Respirar(){
            Console.WriteLine("Estoy respirando");
        }
    }
}
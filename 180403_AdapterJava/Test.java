import java.util.*;

public class Test
{
    public static void main(String[] args)
    {
        ArrayList list = new ArrayList();
        for(int i = 0; i < 10; i++) {
            list.add(i);
        }
        Enumeration<Object> enumeration = Collections.enumeration(list);
        
        Adapter iterator = new Adapter(enumeration);
        while(iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }
}
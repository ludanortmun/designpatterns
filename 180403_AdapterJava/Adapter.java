import java.util.*;

public class Adapter implements Iterator<Object>
{
    private Enumeration<Object> enumeration;

    public Adapter(Enumeration<Object> enumeration)
    {
        this.enumeration = enumeration;
    }

    public boolean hasNext()
    {
        return enumeration.hasMoreElements();
    }

    public Object next()
    {
        return enumeration.nextElement();
    }

    public void remove()
    {
        throw new UnsupportedOperationException();
    }
}
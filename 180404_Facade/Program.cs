﻿using System;

namespace _180404_Facade
{
    class Program
    {
        static void Main(string[] args)
        {
            TV tv = new TV();
            DVD player = new DVD();
            Light light = new Light();

            Facade fachada = new Facade(tv, player, light);
            fachada.watchMovie("Awesome movie");
            fachada.bathroomBreak();
            fachada.changeMovie("Even more awesome movie");
        }
    }
}

using System;

namespace _180404_Facade
{
    public class Light 
    {
        public void on()
        {
            Console.WriteLine("Lights on");
        }

        public void off()
        {
            Console.WriteLine("Lights out");
        }

        public void dim()
        {
            Console.WriteLine("Lights dimmed");
        }
    }
}
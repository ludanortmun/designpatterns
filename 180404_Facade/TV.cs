using System;

namespace _180404_Facade
{
    public class TV
    {
        public void on()
        {
            Console.WriteLine("TV on");
        }

        public void off()
        {
            Console.WriteLine("TV off");
        }
    }
}
using System;

namespace _180404_Facade
{
    public class DVD
    {
        public string Movie;

        public DVD(string movie = "")
        {
            Movie = movie;
        }

        public void on()
        {
            Console.WriteLine("DVD on");
        }

        public void off()
        {
            Console.WriteLine("DVD off");
        }

        public void eject()
        {
            Movie = string.Empty;
            Console.WriteLine("Movie ejected");
        }

        public void insertMovie(string movie) 
        {
            Movie = movie;
            Console.WriteLine("{0} is in the DVD tray", movie);
        }

        public void play()
        {
            if (Movie != string.Empty) {
                Console.WriteLine("Playing {0}", Movie);
            } else {
                Console.WriteLine("No movie is playing");
            }
        }

        public void pause()
        {
            Console.WriteLine("Paused");
        }
    }
}
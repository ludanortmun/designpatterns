using System;

namespace _180404_Facade
{
    public class Facade
    {
        public TV tv;
        public DVD player;
        public Light light;

        public Facade(TV tv, DVD player, Light light)
        {
            this.tv = tv;
            this.player = player;
            this.light = light;
        }

        public void changeMovie(string movie)
        {
            player.eject();
            player.insertMovie(movie);
            player.play();
        }

        public void watchMovie(string movie)
        {
            tv.on();
            player.on();
            player.insertMovie(movie);
            light.off();
            player.play();
        }

        public void bathroomBreak()
        {
            player.pause();
            light.on();
            Console.WriteLine("---ACTUAL BATHROOM BREAK---");
            light.off();
            player.play();
        }
    }
}
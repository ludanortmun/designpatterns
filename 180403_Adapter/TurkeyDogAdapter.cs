using System;

namespace _180403_Adapter
{
    public class TurkeyDogAdapter : IDog
    {
        Turkey turkey;

        public TurkeyDogAdapter(Turkey turkey)
        {
            this.turkey = turkey;
        }

        public void bite()
        {
            turkey.peck();
        }

        public void bark()
        {
            turkey.gobble();
        }
    }
}
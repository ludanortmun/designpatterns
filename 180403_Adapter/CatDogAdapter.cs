using System;

namespace _180403_Adapter
{
    public class CatDogAdapter : IDog
    {
        private Cat cat;
        public CatDogAdapter(Cat cat)
        {
            this.cat = cat;
        }

        public void bark()
        {
            cat.purr();
        }

        public void bite()
        {
            cat.slash();
        }
    }
}
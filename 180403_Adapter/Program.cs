﻿using System;

namespace _180403_Adapter
{
    class Program
    {
        static void Main(string[] args)
        {
            Cat cat = new Cat();
            IDog catDogAdapter = new CatDogAdapter(cat);
            catDogAdapter.bark();
            catDogAdapter.bite();

            Turkey turkey = new Turkey();
            IDog turkeyDogAdapter = new TurkeyDogAdapter(turkey);
            turkeyDogAdapter.bark();
            turkeyDogAdapter.bite();
        }
    }
}

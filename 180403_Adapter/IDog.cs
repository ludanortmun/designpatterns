namespace _180403_Adapter
{
    public interface IDog
    {
        void bark();
        void bite();
    }
}
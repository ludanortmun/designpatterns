using System;

namespace _180403_Adapter
{
    public class Turkey
    {
        public void peck()
        {
            Console.WriteLine("Pecking");
        }

        public void gobble()
        {
            Console.WriteLine("Gobble gobble gobble");
        }
    }
}
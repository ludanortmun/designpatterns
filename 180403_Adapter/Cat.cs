using System;

namespace _180403_Adapter
{
    public class Cat
    {
        public void purr()
        {
            Console.WriteLine("Purring");
        }

        public void slash()
        {
            Console.WriteLine("Slashing");
        }
    }
}